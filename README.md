# gin-admin-cli - [gin-admin](https://gitee.com/forging2012/gin-admin)

> GinAdmin辅助工具

## 下载并使用

```text
go get -u -v github.com/forging2012/gin-admin-cli
go get -u -v gitee.com/forging2012/gin-admin-cli

```

## git 操作

```text
git pull origin master
git pull forging2012 master
```

删除本地tag
git tag -d v20190514

删除远程tag
git push github :refs/tags/v1.0.0


### 创建项目

```text
USAGE:
   gin-admin-cli new [command options] [arguments...]

OPTIONS:
   --dir value, -d value  项目生成目录
   --pkg value, -p value  项目包名
   --mirror, -m           使用国内镜像(gitee.com)
   --web, -w              包含web项目(下载gin-admin-react项目到web目录)
```

> 使用示例


更新库工具


go get -u -v gitee.com/forging2012/gin-admin-cli && ls -al `which  gin-admin-cli` && gin-admin-cli -h

```text
go get -u -v gitee.com/forging2012/gin-admin-cli

ls -al `which  gin-admin-cli` && gin-admin-cli -h

```

```text
gin-admin-cli new -m -d ~/go/src/gin-admin-test -p gin-admin-test
gin-admin-cli new -m -d ~/go/src/gin-admin-test -p gin-admin-test -w
gin-admin-cli new -m -d ~/go/src/gin-admin-test1 -p gin-admin-test1
gin-admin-cli new -m -d ~/go/src/gin-admin-test2 -p gin-admin-test2
gin-admin-cli new -m -d ~/go/src/gin-admin-test3 -p gin-admin-test3
```

```text
gin-admin-cli new -d ~/go/src/gin-admin-test -p gin-admin-test
gin-admin-cli new -m -d ~/go/src/gin-admin-test -p gin-admin-test
gin-admin-cli new -m -d ~/go/src/gin-admin-test -p gin-admin-test -w
git clone -q -b master https://gitee.com/forging2012/gin-admin.git /Users/songyawei/go/src/gin-admin-test
```


创建示例项目

```text
➜  ~ gin-admin-cli new -m -d ~/go/src/test-gin-admin -p test-gin-admin
2020/02/20 00:05:47 项目生成目录：/Users/songyawei/go/src/test-gin-admin
2020/02/20 00:05:47 执行命令：git clone -q -b master https://gitee.com/forging2012/gin-admin.git /Users/songyawei/go/src/test-gin-admin

项目创建成功：/Users/songyawei/go/src/test-gin-admin

├── cmd
│   └── server：主服务
├── configs：配置文件目录
├── docs：文档目录
├── internal：内部应用
│   └── app：主应用目录
│       ├── bll：业务逻辑层接口
│       │   └── impl：业务逻辑层的接口实现
│       ├── config：配置参数（与配置文件一一映射）
│       ├── context：统一上下文
│       ├── errors：统一的错误定义
│       ├── ginplus：gin的扩展函数库
│       ├── middleware：gin中间件
│       ├── model：存储层接口
│       │   └── impl：存储层接口实现
│       ├── routers：路由层
│       │   └── api：/api路由模块
│       │       └── ctl：/api路由模块对应的控制器层
│       ├── schema：对象模型
│       ├── swagger：swagger静态目录
│       └── test：单元测试
├── pkg：公共模块
│   ├── auth：认证模块
│   │   └── jwtauth：JWT认证模块实现
│   ├── gormplus：gorm扩展实现
│   ├── logger：日志模块
│   └── util：工具库
└── scripts：执行脚本

```


### 生成业务模块

#### 指定模块名称和说明生成模块

```text
USAGE:
   gin-admin-cli generate [command options] [arguments...]

OPTIONS:
   --dir value, -d value      项目生成目录
   --pkg value, -p value      项目包名
   --ctl value                控制器swagger模板(支持default(基于github.com/swaggo/swag)和tb(基于github.com/teambition/swaggo)) (default: "default")
   --router value             路由模块(routers/api/api.go) (default: "api")
   --name value, -n value     业务模块名称(结构体名称)
   --comment value, -c value  业务模块注释(结构体注释)
   --file value, -f value     指定模板文件(.json，模板配置可参考说明)
   --module value, -m value   指定生成模块（以逗号分隔，支持：all,schema,entity,model,bll,ctl,api）
```

> 使用示例

```text
gin-admin-cli g -d ./test-gin-admin -p test-gin-admin -n Task -c '任务管理'
```


#### 指定配置文件生成模块

```text
gin-admin-cli g -d 项目目录 -p 包名 -f 配置文件(json)
```

> 配置文件说明

```json
{
  "struct_name": "结构体名称",
  "comment": "结构体注释说明",
  "fields": [
    {
      "struct_field_name": "结构体字段名称",
      "comment": "结构体字段注释",
      "struct_field_required": "结构体字段是否是必选项",
      "struct_field_type": "结构体字段类型",
      "gorm_options": "gorm配置项",
      "binding_options": "binding配置项（不包含required，required由struct_field_required控制）"
    }
  ]
}
```

> 使用示例

> 创建`task.json`文件

```json
{
  "struct_name": "Task",
  "comment": "任务管理",
  "fields": [
    {
      "struct_field_name": "RecordID",
      "comment": "记录ID",
      "struct_field_required": false,
      "struct_field_type": "string",
      "gorm_options": "size:36;index;",
      "binding_options": ""
    },
    {
      "struct_field_name": "Name",
      "comment": "任务名称",
      "struct_field_required": true,
      "struct_field_type": "string",
      "gorm_options": "size:200;index;",
      "binding_options": ""
    },
    {
      "struct_field_name": "Memo",
      "comment": "备注",
      "struct_field_required": false,
      "struct_field_type": "string",
      "gorm_options": "size:1024;",
      "binding_options": ""
    },
    {
      "struct_field_name": "Status",
      "comment": "状态(1:启用 2:停用)",
      "struct_field_required": true,
      "struct_field_type": "int",
      "gorm_options": "",
      "binding_options": "max=2,min=1"
    },
    {
      "struct_field_name": "Creator",
      "comment": "创建者",
      "struct_field_required": false,
      "struct_field_type": "string",
      "gorm_options": "size:36;index;",
      "binding_options": ""
    }
  ]
}
```

```text
gin-admin-cli g -d ./test-gin-admin -p test-gin-admin -f task.json
```

## MIT License

    Copyright (c) 2019 Lyric
