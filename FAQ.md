# FAQ








### go module使用教程:使用go mod的方法.报错go: cannot determine module path for source directory E:\tttttt (outside
2020年02月26日10:25:05

```text
go module使用教程:使用go mod的方法.报错go: cannot determine module path for source directory E:\tttttt (outside

2019-03-11
使用go mod的方法

单独从大工程里拷出小文件想新建新文件工程时，部分文件路径名报红，出现报错
go: cannot determine module path for source directory E:\tttttt (outside GOPATH, no import comments)
1.在src 目录下新建 go.mod文件
2. go.mod中第一行加入一行内容 module src
3.goland中 file->Settings->Go->Go Modules中勾上 Enable Go Modules(vgo) integration

右键src目录，以goland工程打开，报红消失

```


### go version go1.14 darwin/amd64
2020年02月26日10:00:00

```text
➜  ~ go version
go version go1.14 darwin/amd64

➜  ~ go get -u -v gitee.com/forging2012/gin-admin@v0.1.2
get "gitee.com/forging2012/gin-admin": found meta tag get.metaImport{Prefix:"gitee.com/forging2012/gin-admin", VCS:"git", RepoRoot:"https://gitee.com/forging2012/gin-admin.git"} at //gitee.com/forging2012/gin-admin?go-get=1
get "gitee.com/forging2012": found meta tag get.metaImport{Prefix:"gitee.com/forging2012", VCS:"git", RepoRoot:"https://gitee.com/forging2012"} at //gitee.com/forging2012?go-get=1
go: downloading gitee.com/forging2012/gin-admin v0.1.2
```

### module declares its path as: github.com/LyricTian/captcha
2020年02月25日21:03:01
```text
➜  gin-admin git:(master) ✗ ./run.sh 
go run cmd/server/main.go -c ./configs/config.toml -m ./configs/model.conf -swagger ./docs/swagger -menu ./configs/menu.json
go: github.com/forging2012/captcha@v1.1.0: parsing go.mod:
	module declares its path as: github.com/LyricTian/captcha
	        but was required as: github.com/forging2012/captcha
make: *** [start] Error 1

```


### git push -u origin master -f
2020年02月25日18:18:00

```text
➜  gin-admin-cli git:(master) git push -u origin master

fatal: 'gitee.com/forging2012/gin-admin-cli' does not appear to be a git repository
fatal: Could not read from remote repository.

Please make sure you have the correct access rights
and the repository exists.
➜  gin-admin-cli git:(master) git remote add origin git@gitee.com:forging2012/gin-admin-cli.git
fatal: remote origin already exists.
➜  gin-admin-cli git:(master) git remote rm origin 
➜  gin-admin-cli git:(master) git remote add origin git@gitee.com:forging2012/gin-admin-cli.git
➜  gin-admin-cli git:(master) git push -u origin master

To gitee.com:forging2012/gin-admin-cli.git
 ! [rejected]        master -> master (fetch first)
error: failed to push some refs to 'git@gitee.com:forging2012/gin-admin-cli.git'
hint: Updates were rejected because the remote contains work that you do
hint: not have locally. This is usually caused by another repository pushing
hint: to the same ref. You may want to first integrate the remote changes
hint: (e.g., 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.

➜  gin-admin-cli git:(master) git push -u origin master -f

Counting objects: 30, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (29/29), done.
Writing objects: 100% (30/30), 20.32 KiB | 0 bytes/s, done.
Total 30 (delta 4), reused 0 (delta 0)
remote: Powered by GITEE.COM [GNK-3.8]
To gitee.com:forging2012/gin-admin-cli.git
 + 65ce728...55aa094 master -> master (forced update)
Branch master set up to track remote branch master from origin.
```


### go get: gitee.com/forging2012/gin-admin-cli@v1.3.5: parsing go.mod: go.mod:6: invalid module path
2020年02月25日17:26:32




2020年02月25日17:45:45
```text
➜  gin-admin-cli git:(master) ✗ go mod tidy
go: gin-admin-cli imports
	gitee.com/forging2012/gin-admin-cli/cmd: gitee.com/forging2012/gin-admin-cli@v1.3.5: parsing go.mod: go.mod:6: invalid module path
```


2020年02月25日17:45:41
```text
➜  ~ go get -u -v gitee.com/forging2012/gin-admin-cli          
go get: gitee.com/forging2012/gin-admin-cli@v1.3.5: parsing go.mod: go.mod:6: invalid module path
➜  ~ 
```


### invalid version: module contains a go.mod file, so major version must be compatible: should be v0 or v1, not v4
2020年02月25日16:56:36
https://zhuanlan.zhihu.com/p/101529636



gitee.com/forging2012/gin-admin-cli/v1 v1.3.5 // indirect

```text
➜  ~ go get -u -v gitee.com/forging2012/gin-admin-cli/v1@v1.3.5
go: finding gitee.com v1.3.5
go: finding gitee.com/forging2012/gin-admin-cli v1.3.5
go: finding gitee.com/forging2012 v1.3.5
go: downloading gitee.com/forging2012/gin-admin-cli v1.3.5
go: extracting gitee.com/forging2012/gin-admin-cli v1.3.5
go get gitee.com/forging2012/gin-admin-cli/v1@v1.3.5: module gitee.com/forging2012/gin-admin-cli@v1.3.5 found, but does not contain package gitee.com/forging2012/gin-admin-cli/v1
➜  ~ 

```


2020年02月25日17:02:51
```text
not found: gitee.com/forging2012/gin-admin-cli/v4@v4.0.1: unrecognized import path "gitee.com/forging2012/gin-admin-cli/v4" (parse https://gitee.com/forging2012/gin-admin-cli/v4?go-get=1: no go-import meta tags ())
```

2020年02月25日16:58:28
```text
➜  ~ go get -u -v gitee.com/forging2012/gin-admin-cli/4@v4.0.1 
go: finding gitee.com v4.0.1
go: finding gitee.com/forging2012/gin-admin-cli/4 v4.0.1
go: finding gitee.com/forging2012/gin-admin-cli v4.0.1
go: finding gitee.com/forging2012 v4.0.1
go: finding gitee.com v4.0.1
go: finding gitee.com/forging2012/gin-admin-cli v4.0.1
go: finding gitee.com/forging2012 v4.0.1
go: finding gitee.com/forging2012/gin-admin-cli/4 v4.0.1
get "gitee.com/forging2012/gin-admin-cli": found meta tag get.metaImport{Prefix:"gitee.com/forging2012/gin-admin-cli", VCS:"git", RepoRoot:"https://gitee.com/forging2012/gin-admin-cli.git"} at //gitee.com/forging2012/gin-admin-cli?go-get=1
go: finding gitee.com/forging2012/gin-admin-cli v4.0.1
get "gitee.com/forging2012": found meta tag get.metaImport{Prefix:"gitee.com/forging2012", VCS:"git", RepoRoot:"https://gitee.com/forging2012"} at //gitee.com/forging2012?go-get=1
go: finding gitee.com/forging2012 v4.0.1
go get gitee.com/forging2012/gin-admin-cli/4@v4.0.1: gitee.com/forging2012/gin-admin-cli@v4.0.1: invalid version: module contains a go.mod file, so major version must be compatible: should be v0 or v1, not v4
➜  ~ 
```


2020年02月25日16:58:21
```text
➜  ~ go get -u -v gitee.com/forging2012/gin-admin-cli@v4.0.1
go: finding gitee.com v4.0.1
go: finding gitee.com/forging2012 v4.0.1
go: finding gitee.com/forging2012/gin-admin-cli v4.0.1
go: finding gitee.com v4.0.1
go: finding gitee.com/forging2012 v4.0.1
go: finding gitee.com/forging2012/gin-admin-cli v4.0.1
get "gitee.com/forging2012": found meta tag get.metaImport{Prefix:"gitee.com/forging2012", VCS:"git", RepoRoot:"https://gitee.com/forging2012"} at //gitee.com/forging2012?go-get=1
go: finding gitee.com/forging2012 v4.0.1
get "gitee.com/forging2012/gin-admin-cli": found meta tag get.metaImport{Prefix:"gitee.com/forging2012/gin-admin-cli", VCS:"git", RepoRoot:"https://gitee.com/forging2012/gin-admin-cli.git"} at //gitee.com/forging2012/gin-admin-cli?go-get=1
go: finding gitee.com/forging2012/gin-admin-cli v4.0.1
go get gitee.com/forging2012/gin-admin-cli@v4.0.1: gitee.com/forging2012/gin-admin-cli@v4.0.1: invalid version: module contains a go.mod file, so major version must be compatible: should be v0 or v1, not v4
➜  ~ go get -u -v gitee.com/forging2012/gin-admin-cli/v4@v4.0.1
go: finding gitee.com v4.0.1
go: finding gitee.com/forging2012/gin-admin-cli v4.0.1
go: finding gitee.com/forging2012/gin-admin-cli/v4 v4.0.1
go: finding gitee.com/forging2012 v4.0.1
go: finding gitee.com v4.0.1
go: finding gitee.com/forging2012/gin-admin-cli v4.0.1
go: finding gitee.com/forging2012 v4.0.1
go: finding gitee.com/forging2012/gin-admin-cli/v4 v4.0.1
get "gitee.com/forging2012": found meta tag get.metaImport{Prefix:"gitee.com/forging2012", VCS:"git", RepoRoot:"https://gitee.com/forging2012"} at //gitee.com/forging2012?go-get=1
go: finding gitee.com/forging2012 v4.0.1
get "gitee.com/forging2012/gin-admin-cli": found meta tag get.metaImport{Prefix:"gitee.com/forging2012/gin-admin-cli", VCS:"git", RepoRoot:"https://gitee.com/forging2012/gin-admin-cli.git"} at //gitee.com/forging2012/gin-admin-cli?go-get=1
go: finding gitee.com/forging2012/gin-admin-cli v4.0.1
go get gitee.com/forging2012/gin-admin-cli/v4@v4.0.1: gitee.com/forging2012/gin-admin-cli@v4.0.1: invalid version: module contains a go.mod file, so major version must be compatible: should be v0 or v1, not v4
➜  ~ 

```



### (parse https://gitee.com/forging2012/gin-admin-cli/v4?go-get=1: no go-import meta tags ())
2020年02月25日16:28:18

```text
go: gitee.com/forging2012/gin-admin-cli/v4@v4.0.1: unrecognized import path "gitee.com/forging2012/gin-admin-cli/v4" (parse https://gitee.com/forging2012/gin-admin-cli/v4?go-get=1: no go-import meta tags ())

```

### version "v4.0.1" invalid: module contains a go.mod file, so major version must be compatible: should be v0 or v1, not v4
2020年02月25日16:23:01

```text
➜  gin-admin-cli git:(master) ✗ go run main.go 
go: finding gitee.com/forging2012/gin-admin-cli v4.0.1
go: finding gitee.com/forging2012/gin-admin-cli v4.0.1
go: errors parsing go.mod:
/Users/songyawei/Workspace/Development/goadmin/gin-admin-cli/go.mod:6: require gitee.com/forging2012/gin-admin-cli: version "v4.0.1" invalid: module contains a go.mod file, so major version must be compatible: should be v0 or v1, not v4
```


### module gitee.com/forging2012/gin-admin-cli
2020年02月24日23:35:58

FIX:
module gitee.com/forging2012/gin-admin-cli


go get -u -v gitee.com/forging2012/gin-admin-cli

```text
➜  gin-admin-cli git:(master) ✗ /Users/songyawei/go/bin/gin-admin-cli -v
gin-admin-cli version 0.2.2
```


```text
➜  gin-admin-cli git:(master) ✗ go get -u -v gitee.com/forging2012/gin-admin-cli

gitee.com/forging2012/gin-admin-cli
```



### https://goproxy.io/gitee.com/forging2012/gin-admin-cli/@v/v4.0.1.inf
2020年02月25日16:02:05

```text
go: finding gitee.com/forging2012/gin-admin-cli v4.0.1
go: errors parsing go.mod:
/Users/songyawei/Workspace/Development/goadmin/gin-admin-cli/go.mod:6: require gitee.com/forging2012/gin-admin-cli: reading https://goproxy.io/gitee.com/forging2012/gin-admin-cli/@v/v4.0.1.info: 404 Not Found
```


### require gitee.com/forging2012/gin-admin-cli: version "v3.0.0" invalid: module contains a go.mod file, so major version must be compatible: should be v0 or v1, not v3
2020年02月24日23:24:17

```text
➜  gin-admin-cli git:(master) ✗ go get -u -v gitee.com/forging2012/gin-admin-cli
go: finding gitee.com/forging2012/gin-admin-cli v3.0.0
go: finding gitee.com/forging2012/gin-admin-cli v3.0.0
go: errors parsing go.mod:
/Users/songyawei/Development/goadmin/gin-admin-cli/go.mod:7: require gitee.com/forging2012/gin-admin-cli: version "v3.0.0" invalid: module contains a go.mod file, so major version must be compatible: should be v0 or v1, not v3

```

```text
➜  gin-admin-cli git:(master) ✗ go get -u -v gitee.com/forging2012/gin-admin-cli
go: errors parsing go.mod:
/Users/songyawei/Development/goadmin/gin-admin-cli/go.mod:7: no matching versions for query "v3"
```



### 清空本地缓存代码
2020年02月24日10:17:06

go get -u -v gitee.com/forging2012/gin-admin-cli

```text
➜  pkg find . -name "*gin-admin-cli*"
./mod/cache/download/sumdb/sum.golang.org/lookup/github.com/!lyric!tian/gin-admin-cli@v1.0.0
./mod/cache/download/gitee.com/forging2012/gin-admin-cli
./mod/cache/download/github.com/!lyric!tian/gin-admin-cli
./mod/gitee.com/forging2012/gin-admin-cli@v1.0.0
./mod/github.com/!lyric!tian/gin-admin-cli@v1.0.0
```

清空本地缓存代码
```text
sudo rm -rf /Users/songyawei/go/pkg/mod/gitee.com/forging2012/gin-admin-cli*
sudo rm -rf /Users/songyawei/go/pkg/mod/cache/download/gitee.com/forging2012/gin-admin-cli*

```

### go mod下载依赖错误Get https://sum.golang.org
2020年02月23日23:09:12

go env -w GOSUMDB=off

```text
➜  ~ go get -u -v gitee.com/forging2012/gin-admin-cli
get "gitee.com/forging2012/gin-admin-cli": found meta tag get.metaImport{Prefix:"gitee.com/forging2012/gin-admin-cli", VCS:"git", RepoRoot:"https://gitee.com/forging2012/gin-admin-cli.git"} at //gitee.com/forging2012/gin-admin-cli?go-get=1
get "gitee.com/forging2012": found meta tag get.metaImport{Prefix:"gitee.com/forging2012", VCS:"git", RepoRoot:"https://gitee.com/forging2012"} at //gitee.com/forging2012?go-get=1
go: downloading gitee.com/forging2012/gin-admin-cli v1.0.0
verifying gitee.com/forging2012/gin-admin-cli@v1.0.0: gitee.com/forging2012/gin-admin-cli@v1.0.0: reading https://sum.golang.google.cn/lookup/gitee.com/forging2012/gin-admin-cli@v1.0.0: 410 Gone
➜  ~ which gin-admin-cli
gin-admin-cli not found
➜  ~ go env -w GOSUMDB=off
➜  ~ which gin-admin-cli  
gin-admin-cli not found

➜  ~ go get -u -v gitee.com/forging2012/gin-admin-cli
get "gitee.com/forging2012/gin-admin-cli": found meta tag get.metaImport{Prefix:"gitee.com/forging2012/gin-admin-cli", VCS:"git", RepoRoot:"https://gitee.com/forging2012/gin-admin-cli.git"} at //gitee.com/forging2012/gin-admin-cli?go-get=1
get "gitee.com/forging2012": found meta tag get.metaImport{Prefix:"gitee.com/forging2012", VCS:"git", RepoRoot:"https://gitee.com/forging2012"} at //gitee.com/forging2012?go-get=1
go: downloading gitee.com/forging2012/gin-admin-cli v1.0.0
go: extracting gitee.com/forging2012/gin-admin-cli v1.0.0
gitee.com/forging2012/gin-admin-cli
➜  ~ which gin-admin-cli
/Users/songyawei/go/bin/gin-admin-cli
```

```text
go mod下载依赖错误Get https://sum.golang.org
冰生于水 最后发布于2019-09-30 23:29:52
本文链接：https://blog.csdn.net/qq_38258310/article/details/101801243
升级1.13版本之后下载依赖出现错误：

verifying github.com/gorilla/websocket@v1.4.1/go.mod: github.com/gorilla/websocket@v1.4.1/go.mod: Get https://sum.golang.org/lookup/gi
thub.com/gorilla/websocket@v1.4.1: dial tcp 172.217.24.17:443: connectex: A connection attempt failed because the connected party did
not properly respond after a period of time, or established connection failed because connected host has failed to respond.
原因是1.13之后设置了默认的GOSUMDB=sum.golang.org，由于墙的原因无法访问，执行以下命令关闭即可：

go env -w GOSUMDB=off
```


# END
