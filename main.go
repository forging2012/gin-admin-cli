package main

import (
	"log"
	"os"

	"gitee.com/forging2012/gin-admin-cli/cmd"
	"github.com/urfave/cli"
)

func main() {
	app := cli.NewApp()
	app.Name = "gin-admin-cli"
	app.Description = "GinAdmin辅助工具"
	app.Version = "v1.0.2"
	app.Commands = []cli.Command{
		// NewCommand 创建项目命令
		cmd.NewCommand(),
		// GenerateCommand 生成项目模块命令
		cmd.GenerateCommand(),
	}
	err := app.Run(os.Args)
	if err != nil {
		log.Fatalf(err.Error())
	}
}
