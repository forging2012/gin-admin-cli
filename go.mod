module gitee.com/forging2012/gin-admin-cli

go 1.13

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/jinzhu/inflection v1.0.0
	github.com/urfave/cli v1.22.2
)
